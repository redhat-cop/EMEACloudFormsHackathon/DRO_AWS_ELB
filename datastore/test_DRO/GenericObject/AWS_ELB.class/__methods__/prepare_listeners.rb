#
# Description: <Method description here>
#

me=$evm.root["generic_object"]

listeners=me.listeners

ansible_listeners=""

listeners.each do |listener|
  protocol=listener.protocol
  lb_port=listener.lb_port
  instance_port=listener.instance_port
  ansible_listeners+="- protocol: #{protocol}
  instance_port: #{instance_port}
  load_balancer_port: #{lb_port}
  "
end

$evm.root["dialog_param_listeners"]=ansible_listeners
$evm.log(:info,"listeners=#{ansible_listeners}")
