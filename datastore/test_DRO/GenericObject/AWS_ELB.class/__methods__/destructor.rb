#
# Description: <Method description here>
#

obj=$evm.root["generic_object"]
svc=obj.services.first
$evm.log(:info,"removing #{obj.name} from #{svc.name rescue "none"}")
obj.remove_from_service(svc)
obj.destroy
