#
# Description: <Method description here>
#

me=$evm.root["generic_object"]

svc_id=me.service_id
svc=$evm.vmdb(:service,svc_id)
$evm.log(:info,"attaching #{me.name} to #{svc.name rescue "none"} [#{me.service_id}]")
$evm.root['dialog_param_elb_name'] = me.name
me.services=[svc]
me.add_to_service(svc)
me.save
