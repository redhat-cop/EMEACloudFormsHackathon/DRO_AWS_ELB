#
# Description: <Method description here>
#

me=$evm.root["generic_object"]
sparams=$evm.root["param_1"]
params=eval(sparams) || sparams
$evm.log(:info,"convert #{sparams.inspect} to #{params.inspect}")
if $evm.root["param_1_type"]="Hash"
  protocol=params["protocol"]
  lb_port=params["lb_port"]
  instance_port=params["instance_port"]
else
  protocol="tcp"
  lb_port=22
  instance_port=22
end

name="#{protocol}-#{lb_port}-#{instance_port}"
$evm.log(:info,"creating Listener: #{name}")

dro_class = $evm.vmdb(:generic_object_definition).find_by(:name => "AWS_ELB_listener")
elb = dro_class.create_object(:name => name,:protocol=>protocol, :instance_port=>instance_port,:lb_port=>lb_port)

listeners=me.listeners
listeners.append(elb)
me.listeners=listeners
me.save

elb.constructor("ELB"=>me.id)
