#
# Description: <Method description here>
#

me=$evm.root["generic_object"]
sparams=$evm.root["param_1"]
params=eval(sparams) || sparams
$evm.log(:info,"convert #{sparams.inspect} to #{params.inspect}")

myelb=$evm.vmdb(:generic_object,params["ELB"])

$evm.log(:info,"attaching #{me.name} to #{myelb.name rescue "none"}")

me.elb=[myelb]
me.save
