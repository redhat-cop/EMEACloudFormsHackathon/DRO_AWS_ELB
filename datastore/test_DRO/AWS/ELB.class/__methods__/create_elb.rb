#
# Description: <Method description here>
#

svc=$evm.root["service"]
svc.attributes.each{|k,v| $evm.log(:info,"svc[#{k}]=#{v}")}
svc.options.each{|k,v| $evm.log(:info,"options[#{k}]=#{v}")}
$evm.root.attributes.each{|k,v| $evm.log(:info,"root[#{k}]=#{v}")}

svc=$evm.root["service"]
name="elb-#{svc.name}"
#svc.options["elb_name"] = name
$evm.log(:info,"obj name=#{name}")

dro_class = $evm.vmdb(:generic_object_definition).find_by(:name => "AWS_ELB")
elb = dro_class.create_object(:name => name,"service_id"=>svc.id)
elb.constructor("protocol"=>$evm.root["dialog_param_protocol"],"lb_port"=>$evm.root["dialog_param_lb_port"],"instance_port"=>$evm.root["dialog_param_instance_port"])
