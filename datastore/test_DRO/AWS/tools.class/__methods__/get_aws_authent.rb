#
# Description: <Method description here>
#

$evm.log(:info,"preparing new ELB")
$evm.root.attributes.each{|k,v| $evm.log(:info,"root[#{k}]=#{v}")}

aws=$evm.vmdb(:ext_management_system).find_by_type("ManageIQ::Providers::Amazon::CloudManager")
userid=aws.authentication_userid
pass=aws.authentication_password

$evm.root["dialog_param_aws_userid"] = userid
$evm.root["dialog_param_aws_password"] = pass

svc=$evm.root["generic_object"].services.first
$evm.root["dialog_param_service_id"] = svc.id
