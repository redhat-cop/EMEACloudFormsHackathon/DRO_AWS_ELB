# Hackathon 2018


# Task List

- [x] Create Hackathon repository
- [x] Open bugzilla


# Bugzilla tracking

- [ ] Bug 1594469 - ON_QA - Dialog options are missing when using a custom button and dialog on GenericObject instance  
- [x] Bug 1594480 - ON_QA - Generic objects class accordion is not display when locale is french  
- [x] Bug 1595133 - CLOSED NOTABUG - When creating DRO instance, can't do associations , the resulting is []  
- [ ] Bug 1595175 - ON_QA - Dialog can't be submited  
- [ ] Bug 1594483 - [RFE] Custom buttons on DRO should be visible no matter from you access it  
- [ ] Bug 1595236 - [RFE] DRO constructor and destructor  
- [ ] Bug 1595227 - [RFE] Link DRO to all objects  
- [ ] Bug 1595258 - [RFE] No reason to display methods in the instance page  
- [ ] Bug 1595250 - [RFE] DRO public and private attributes and associations  


# Bugzilla

## Dialog options are missing when using a custom button and dialog on GenericObject instance

https://bugzilla.redhat.com/show_bug.cgi?id=1594469

Dialog options are missing when using a custom button and dialog on GenericObject instance  
Only the default value is visible in $evm.root for radio button ou dropbox  
Any other option_0_xxx are not in $evm.root as dialog_option_0_xxx  

```
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener> -----------------------------------------------------------------------------
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener> Object Attributes:
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener> -----------------------------------------------------------------------------
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener> Root Attributes:
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - ae_next_state : 
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - ae_result : ok
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - ae_retry_server_affinity : false
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - ae_state : execute
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - ae_state_max_retries : 0
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - ae_state_retries : 0
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - ae_state_started : 2018-06-23 11:38:20 UTC
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - ae_state_step : main
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - dialog_option_0_protocol : http
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - generic_object : #<MiqAeMethodService::MiqAeServiceGenericObject:0x000000000b9355c0>
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - generic_object_id : 92000000000009
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - method_name : add_listener
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - miq_group : #<MiqAeMethodService::MiqAeServiceMiqGroup:0x000000000b4f4560>
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - miq_server : #<MiqAeMethodService::MiqAeServiceMiqServer:0x000000000b736af8>
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - miq_server_id : 92000000000001
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - object_name : GenericObject
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - request : any
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - tenant : #<MiqAeMethodService::MiqAeServiceTenant:0x000000000b4f4c40>
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - user : #<MiqAeMethodService::MiqAeServiceUser:0x000000000b4f52f8>
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - user_id : 92000000000001
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener>    - vmdb_object_type : generic_object
INFO -- : Q-task_id([resource_action_92000000000151]) <AEMethod add_listener> -----------------------------------------------------------------------------
```


## Generic objects class accordion is not display when local is french

https://bugzilla.redhat.com/show_bug.cgi?id=1594480

**Description of problem:**

Generic objects class accordion is not display when local is french

**Version-Release number of selected component (if applicable):**

5.9.2

**How reproducible:**

- Set local to French in the UI. Create an generic object class.
- Navigate to Automate -> Generic Objects -> Click on accordion.
- Accordion doesn't disaply anything.

**Steps to Reproduce:**

- Set local to French
- Create an generic object class
- Automate -> Generic Objets
- Accordion doesn't display any class

**Actual results:**

Accordion doesn't display any class

**Expected results:**

Accordion should display all created Generic Object Class


## When creating DRO instance, can't do associations , the resulting is []
https://bugzilla.redhat.com/show_bug.cgi?id=1595133

**Description of problem:**

When creating DRO instance, it is not possible to populate the assiciations as the attributes

**Version-Release number of selected component (if applicable):**

5.9.2

**How reproducible:**

via the rails console  or via the automation

**Steps to Reproduce:**

1. create a DRO class with attributes (attr_1 and attr_2) ans associations (ass_1 and ass_2)
2. create an instance of the newly created DRO class like:  
   dro_class = $evm.vmdb(:generic_object_definition).find_by(:name => "demo_dro")  
   dro = dro_class.create_object(:name => "First Demo DRO",  
   :attr_1 => "xxx",  
   :attr_2 => "yyy",  
   :ass_1 => [vm.id])  
   :ass_2 => [service.id])  
3. check the values of the attributes and associations
   
** Actual results:**

dro.attr1="xxx"  
dro.attr_2="xxx"  
dro.ass_1=[]  
dro.ass_2=[]  

**Expected results:**

dro.attr1="xxx"  
dro.attr_2="xxx"  
dro.ass_1=[<id>]  
dro.ass_2=[<id>]  


## Dialog can't be submited
https://bugzilla.redhat.com/show_bug.cgi?id=1595175

**Description of problem:**

when displaying a dialog froma custom button created from customization, the dialog is displayed with submit+cancel and save+reset+cancel buttons.  
when clicking on submit, nothing happens  
when clicking on save, "not yet implemented" is displayed, so th edialog can't be submitted.  
when the button is created from generic object definition, the submit is working as expected.

**Version-Release number of selected component (if applicable):**

5.9.2

**How reproducible:**

always

**Steps to Reproduce:**

  1. create a custom button from the customization page, based on service with an associated dialog
  2. from a service, click the created button to display the dialog
  3.

**Actual results:**

submit button has no effect, and save button show the "not yet implemented" message

**Expected results:**

only show the submit+cancel buttons and submit is functionnal



# Request For Enhancement

## [RFE] Link DRO to all objects
https://bugzilla.redhat.com/show_bug.cgi?id=1595227

**Description of problem:**

Generic objects can be linked with only services via the add_to_service method.  
It can be usefull to be able to link a generic object to other object like VM, Hosts, datastore...


## [RFE] Custom buttons on DRO should be visible no matter from you access it 
https://bugzilla.redhat.com/show_bug.cgi?id=1594483

**Description of problem:**

Custom buttons are not visible when DRO instance is acceded via  
Automate -> Generic Object -> Object Class -> Instance  
https://10.100.10.4/generic_object/show/92000000000009  
Custom buttons are visible when DRO instance is acceded via  
My service -> service with DRO attach -> Instances -> Dro instance  
https://10.100.10.4/service/show/92000000000006?display=generic_objects&generic_object_id=92000000000009  

**Version-Release number of selected component (if applicable):**

5.9.2

**How reproducible:**

- Create a DRO and attach it to a service
- Naviagte to DRO instance via My service or Automate

**Steps to Reproduce:**

1. Create a DRO Class
2. Instanciate this DRO class
3. Attach this instance to a service
4. Naviagte to DRO instance via My service or Automate

**Actual results:**

Custom buttons are visible only via My service -> Service only

**Expected results:**

Custom buttons on DRO instance should be visible no matter from you access it

## [RFE] DRO constructor and destructor
https://bugzilla.redhat.com/show_bug.cgi?id=1595236

**Description of problem:**

when instanciating a generic object via dro_class.create, it is possible only to pass attributes (and associations cf bz 1595133), then we have to run a dro method to do all the initialization of the instance.  
It can be usefull to have a custom method launched when a dro instance is created.  
As well, when an instance is destroyed (via the destroy method cf 1595149), it should be usefull to call a destructor custom method to clean and unregisters things.

## [RFE] DRO public and private attributes and associations
https://bugzilla.redhat.com/show_bug.cgi?id=1595250

**Description of problem:**

when displaying a generic object instance, we can see all the attributes, associations. Some of them are exclusively technical or unsafe to show (like password, key ...) and useless for the user.  
It will be usefull to indicate for each attributes and associations if it is public visible or not (public or private) to determine if it has to be displayed or not.

##  [RFE] No reason to display methods in the instance page
https://bugzilla.redhat.com/show_bug.cgi?id=1595258

**Description of problem:**

When displaying generic object instance, all the custom methods are shown. This is absolutely useless for the user.  
This information group have to be hidden
